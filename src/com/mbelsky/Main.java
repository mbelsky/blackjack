package com.mbelsky;

import com.mbelsky.controller.GameController;

public class Main {

    public static void main(String[] args) {
		new GameController().play();
	}
}
