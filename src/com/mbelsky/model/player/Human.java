package com.mbelsky.model.player;

import com.mbelsky.view.DialogWindow;

/**
 * User: mbelsky
 * Date: 05.10.13
 * Time: 21:10
 */
public class Human extends Player {

	private static int PLAYER_ID = 1;

	private final int playerId;

	public Human(final DialogWindow dialogWindow) {
		super( dialogWindow);
		playerId = PLAYER_ID;
		PLAYER_ID++;
	}

	@Override
	public boolean hitMe() {
		while ( true ) {
			final String answer = dialogWindow.getAnswer( String.format( "%s: ", getName()));
			if ( ANSWER_YES.equals( answer) ) {
				return true;
			} else if ( ANSWER_NO.equals( answer) ) {
				return false;
			}

			dialogWindow.print( "Incorrect answer. Please retry.");
		}
	}

	@Override
	public String getName() {
		return String.format( "Player #%d", playerId);
	}
}
