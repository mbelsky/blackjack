package com.mbelsky.model.player;

import com.mbelsky.model.Card;
import com.mbelsky.tools.ShufflingMachine;
import com.mbelsky.view.DialogWindow;

/**
 * User: mbelsky
 * Date: 05.10.13
 * Time: 18:50
 */
public class Dealer extends Bot {

	private boolean secondCardShowed;

	public Dealer(final DialogWindow dialogWindow) {
		super(dialogWindow);
	}

	public void refreshCardDesks() {
		ShufflingMachine.refreshCardDesks();
	}

	public Card getCard() {
		return ShufflingMachine.getCard();
	}

	public boolean isSecondCardShowed() {
		return secondCardShowed;
	}

	public void setSecondCardShowed(final boolean secondCardShowed) {
		this.secondCardShowed = secondCardShowed;
	}

	@Override
	public void throwCards() {
		super.throwCards();
		secondCardShowed = false;
	}

	@Override
	public String getName() {
		return "Dealer";
	}
}
