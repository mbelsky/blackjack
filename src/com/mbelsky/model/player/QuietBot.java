package com.mbelsky.model.player;

import com.mbelsky.view.DialogWindow;

/**
 * User: mbelsky
 * Date: 07.10.13
 * Time: 12:36
 */
public class QuietBot extends Bot {

	public QuietBot(final DialogWindow dialogWindow) {
		super(dialogWindow);
	}

	@Override
	public boolean hitMe() {
		final boolean result = ( 15 >= score ) || 0.3 >= Math.random();
		hitMe( result);
		return result;
	}
}
