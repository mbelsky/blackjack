package com.mbelsky.model.player;

import com.mbelsky.view.DialogWindow;

/**
 * User: mbelsky
 * Date: 07.10.13
 * Time: 12:42
 */
public class GamblerBot extends Bot {

	public GamblerBot(final DialogWindow dialogWindow) {
		super(dialogWindow);
	}

	@Override
	public boolean hitMe() {
		final boolean result = ( 18 >= score ) || 0.7 < Math.random();
		hitMe( result);
		return result;
	}
}
