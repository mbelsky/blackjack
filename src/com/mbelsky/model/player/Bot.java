package com.mbelsky.model.player;

import com.mbelsky.view.DialogWindow;

/**
 * User: mbelsky
 * Date: 05.10.13
 * Time: 18:50
 */
public class Bot extends Player {

	private static int BOT_ID = 1;

	private final int botId;

	public Bot(final DialogWindow dialogWindow) {
		super(dialogWindow);
		botId = BOT_ID;
		BOT_ID++;
	}

	@Override
	public boolean hitMe() {
		final boolean result = (17 > score);
		hitMe( result);

		return result;
	}

	protected void hitMe(final boolean hitMe) {
		dialogWindow.print( String.format( "%s: %s", getName(), hitMe ? ANSWER_YES : ANSWER_NO));
	}

	@Override
	public String getName() {
		return String.format( "Bot #%d", botId);
	}
}
