package com.mbelsky.model.player;

import com.mbelsky.model.Card;
import com.mbelsky.view.DialogWindow;

import java.util.ArrayList;
import java.util.List;

/**
 * User: mbelsky
 * Date: 05.10.13
 * Time: 17:44
 */
public abstract class Player {

	protected static final int DEFAULT_BET_VALUE = 10;
	protected static final String ANSWER_YES = "y";
	protected static final String ANSWER_NO = "n";

	protected final DialogWindow dialogWindow;
	protected int score;
	protected int aceCount;
	protected final List<Card> cards = new ArrayList<Card>();

	public Player(final DialogWindow dialogWindow) {
		this.dialogWindow = dialogWindow;
	}

	public void fetchCard(final Card card) {
		if ( null == card ) {
			throw new IllegalArgumentException( "Card cannot be null");
		}
		cards.add( card);
		if ( card.getName() == Card.Name.ACE ) {
			aceCount++;
		}
		changeScore( card);
	}

	public boolean haveAce() {
		return 0 != aceCount;
	}

	public boolean useAce() {
		final boolean haveAce = haveAce();

		if ( haveAce ) {
			aceCount--;
			for (final Card card : cards) {
				if ( card.getName() != Card.Name.ACE ) {
					continue;
				}
				card.setValue( 1);
				break;
			}
			reCalcScore();
		}

		return haveAce;
	}

	private void reCalcScore() {
		score = 0;
		for (final Card card : cards) {
			score += card.getValue();
		}
	}

	private void changeScore(final Card card) {
		score += card.getValue();
	}

	public int getBet() {
		return DEFAULT_BET_VALUE;
	}

	public int getScore() {
		return score;
	}

	public List<Card> getCards() {
		return new ArrayList<Card>( cards);
	}

	public abstract boolean hitMe();

	public abstract String getName();

	public void throwCards() {
		cards.clear();
		aceCount = 0;
		reCalcScore();
	}
}
