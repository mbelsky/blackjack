package com.mbelsky.model;

/**
 * User: mbelsky
 * Date: 05.10.13
 * Time: 18:51
 */
public class Card {

	public static final int SUITS_COUNT = 4;
	public static final int CARDS_IN_SUIT = 13;
	public static final int CARDS_IN_DESK = SUITS_COUNT * CARDS_IN_SUIT;

	private final Suit suit;
	private final Name name;
	private int value;

	public Card(final Suit suit, final Name name) {
		if ( null == suit || null == name ) {
			throw new IllegalArgumentException( "Invalid initialized values.");
		}
		this.suit = suit;
		this.name = name;

		switch ( name ) {
			case TWO:
			case THREE:
			case FOUR:
			case FIVE:
			case SIX:
			case SEVEN:
			case EIGHT:
			case NINE: {
				value = Integer.parseInt( name.toString());
				break;
			}
			case TEN:
			case JACK:
			case QUEEN:
			case KING: {
				value = 10;
				break;
			}
			case ACE: {
				value = 11;
				break;
			}
			default: throw new IllegalArgumentException( "Unknown name type");
		}
	}

	public Name getName() {
		return name;
	}

	public Suit getSuit() {
		return suit;
	}

	public int getValue() {
		return value;
	}

	public void setValue(final int value) {
		if ( Name.ACE != name ) {
			throw new RuntimeException( "'Value' field cannot be changed for this 'name'.");
		}
		this.value = value;
	}

	@Override
	public String toString() {
		return name.toString() + suit.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if ( this == o ) {
			return true;
		}
		if ( !(o instanceof Card) ) {
			return false;
		}

		final Card card = (Card)o;

		if ( value != card.value ) {
			return false;
		}
		if ( name != card.name ) {
			return false;
		}
		if ( suit != card.suit ) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = suit.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + value;
		return result;
	}

	public enum Suit {
		SPADES( "\u2660"),
		HEARTS( "\u2665"),
		DIAMONDS( "\u2666"),
		CLUBS( "\u2663");

		private final String symbol;
		private Suit(final String symbol) {
			this.symbol = symbol;
		}

		@Override
		public String toString() {
			return symbol;
		}
	}

	public enum Name {
		ACE( "A"),
		TWO( "2"),
		THREE( "3"),
		FOUR( "4"),
		FIVE( "5"),
		SIX( "6"),
		SEVEN( "7"),
		EIGHT( "8"),
		NINE( "9"),
		TEN( "10"),
		JACK( "J"),
		QUEEN ( "Q"),
		KING ( "K");

		private final String symbol;
		Name( final String symbol) {
			this.symbol = symbol;
		}

		@Override
		public String toString() {
			return symbol;
		}
	}
}
