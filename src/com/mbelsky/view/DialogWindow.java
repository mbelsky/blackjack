package com.mbelsky.view;

import java.util.Scanner;

/**
 * User: mbelsky
 * Date: 06.10.13
 * Time: 18:26
 */
public class DialogWindow {

	private static final Scanner scanner = new Scanner( System.in);

	public void print(final String message) {
		System.out.println( message);
	}

	public String getAnswer(final String message) {
		if ( null != message && !"".equals( message) ) {
			System.out.print( message);
		}
		return scanner.next();
	}
}
