package com.mbelsky.tools;

import com.mbelsky.model.Card;

import java.util.ArrayList;
import java.util.List;

/**
 * User: mbelsky
 * Date: 05.10.13
 * Time: 20:38
 */
public class ShufflingMachine {

	private static final int CARD_DESKS_COUNT = 1;

	private static final List<Card> cards = new ArrayList<Card>( Card.CARDS_IN_DESK * CARD_DESKS_COUNT);

	public static Card getCard() {
		//refresh card

		return cards.remove( 0);
	}

	public static void refreshCardDesks() {
		final int cardsCount = CARD_DESKS_COUNT * Card.CARDS_IN_DESK;
		if ( (cardsCount - cards.size()) >= (cardsCount / 3 * 2) ) {
			cards.clear();
			for (int i = 0; i < CARD_DESKS_COUNT; i++) {
				cards.addAll( CardGenerator.generateCardDesk());
			}
		}
	}
}
