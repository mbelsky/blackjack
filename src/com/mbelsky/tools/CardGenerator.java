package com.mbelsky.tools;

import com.mbelsky.model.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: mbelsky
 * Date: 05.10.13
 * Time: 19:40
 */
public class CardGenerator {

	public static List<Card> generateCardDesk() {
		final List<Card> cardDesc = new ArrayList<Card>( Card.CARDS_IN_DESK);
		for (int _suit = 1; _suit <= Card.SUITS_COUNT; _suit++ ) {
			final Card.Suit suit = getSuit( _suit);
			for (int _name = 1; _name <= Card.CARDS_IN_SUIT; _name++) {
				final Card.Name name = getName( _name);
				cardDesc.add( new Card( suit, name));
			}
		}

		Collections.shuffle( cardDesc);
		return cardDesc;
	}

	private static Card.Suit getSuit(final int suit) {
		switch ( suit ) {
			case 1: {
				return Card.Suit.DIAMONDS;
			}
			case 2: {
				return Card.Suit.HEARTS;
			}
			case 3: {
				return Card.Suit.SPADES;
			}
			case 4: {
				return Card.Suit.CLUBS;
			}
			default: return null;
		}
	}

	private static Card.Name getName(final int name) {
		switch ( name ) {
			case 1: {
				return Card.Name.ACE;
			}
			case 2: {
				return Card.Name.TWO;
			}
			case 3: {
				return Card.Name.THREE;
			}
			case 4: {
				return Card.Name.FOUR;
			}
			case 5: {
				return Card.Name.FIVE;
			}
			case 6: {
				return Card.Name.SIX;
			}
			case 7: {
				return Card.Name.SEVEN;
			}
			case 8: {
				return Card.Name.EIGHT;
			}
			case 9: {
				return Card.Name.NINE;
			}
			case 10: {
				return Card.Name.TEN;
			}
			case 11: {
				return Card.Name.JACK;
			}
			case 12: {
				return Card.Name.QUEEN;
			}
			case 13: {
				return Card.Name.KING;
			}
			default: return null;
		}
		//</pain>
	}
}
